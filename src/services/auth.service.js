const { InvalidPasswordError, InvalidSidError } = require('../errors')
const uuidv4 = require('uuid/v4')

module.exports = {
	name: 'auth',

	actions: {
		login: {
			params: {
				username: 'string',
				password: 'string'
			},
			handler(ctx) {
                if (ctx.params.password == this.PASSWORD) {
					let length = this.db_sid.push(uuidv4())
					console.log(`${username} logged in.`)
                    return { sid: this.db_sid[length-1] }
				}
                else
                    throw InvalidPasswordError(ctx.params.password, this.PASSWORD)
			}
		},

		verifySid: {
			params: {
				sid: 'string'
			},
			handler(ctx) {
				if (this.db_sid.includes(ctx.params.sid))
					return ctx.params.sid
				else
					throw InvalidSidError()
			}
		}
	},

	created() {
		this.db_sid = []
		this.PASSWORD = "microservices"
	}
}
