module.exports = {
	name: 'calculator',

	actions: {
		sum: {
			params: {
				x: 'number',
				y: 'number'
			},
			handler(ctx) {
				let { x, y } = ctx.params
				console.log(`Sum: x + y = ${x + y}`)
				return { result: x + y }
			}
        }
	}
}
