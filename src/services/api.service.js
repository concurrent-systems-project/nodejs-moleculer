const { MissingSidError } = require('../errors')
const ApiGateway = require('moleculer-web')

module.exports = {
    name: 'api',

    mixins: [ ApiGateway ],

    settings: {
        port: 9000,
        routes: [
            {
                path: '/',
                bodyParsers: { json: true },
                authorization: true,
				whitelist: [ "auth.login", "calculator.*" ],
                aliases: {
                    "POST login": 'auth.login',
                    "POST sum": 'calculator.sum'
                }
            }
        ]
    },

    methods: {
        async authorize(ctx, route, req) {
            if (req.originalUrl != '/login') {
                if (req.body.sid)
                    ctx.meta.sid = await ctx.call('auth.verifySid', { sid: req.body.sid })
                else
                    throw MissingSidError()
            }
        }
    }
}
