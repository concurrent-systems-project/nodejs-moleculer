const { MoleculerError } = require("moleculer").Errors

function logError(err) {
    console.log(`Error: ${err.type}`)
    return err
}

module.exports = {
    MissingSidError: () => logError(new MoleculerError(
        message = "Sid required, you must specify your sid in the  request's body.",
        code = 401,
        type = 'MISSING_SID'
    )),

    InvalidSidError: () => logError(new MoleculerError(
        message = "Invalid sid, please login again to receive a new one.",
        code = 401,
        type = 'INVALID_SID'
    )),

    InvalidPasswordError: (password, password_valid) => logError(new MoleculerError(
        message = "Invalid password, please try again.",
        code = 417,
        type = 'INVALID_PASSWORD',
        data = { entered: password, password: password_valid }
    ))
}
