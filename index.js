const { ServiceBroker } = require('moleculer')

const broker = new ServiceBroker({ logLevel: "fatal" })
broker.loadServices('./src/services')
broker.start().then(() => { console.log("Server started") })
