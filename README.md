![Moleculer logo](http://moleculer.services/images/banner.png)

# Node.js - Moleculer

The goal of this project is to set a [Node.js](https://nodejs.org/en/) example of **microservices** using the [Moleculer](https://github.com/moleculerjs/moleculer) framework.
For that purpose we create a **calculator** using an authentification system.

Here is a list of the npm packages used:
- [moleculer](https://github.com/moleculerjs/moleculer)
- [moleculer-web](https://github.com/moleculerjs/moleculer-web)
- [uuid](https://github.com/kelektiv/node-uuid) (for sid generation)


# Features

- Rest-API
- HTTP requests
- Authentification login via common password
- Authorization via sid in the body of the request
- Calculator operations (only sum)

# HTTP Requests

The following HTTP requests are based on the **localhost** IP Adresse: **http://localhost:9000**

### POST /login

Login request where you specify a username and the password.
The authentification system is not link to a database, so the username can be random and the password is common for everyone.

Returns a sid if the password is correct.

| Parameter   | Type     | Description                      |
| :---------: | :------: | -------------------------------- |
| `username`  | `String` | Any username would work          |
| `password`  | `String` | Common password: 'microservices' |

### POST /sum

Sum request between two numbers.

Returns the result of (x + y).

| Parameter   | Type     | Description              |
| :---------: | :------: | ------------------------ |
| `x`         | `Number` | Any integer or float     |
| `y`         | `Number` | Any integer or float     |
| `sid`       | `String` | Sid received after login |
